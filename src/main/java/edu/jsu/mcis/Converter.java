package edu.jsu.mcis;

import java.io.*;
import java.util.*;
import java.lang.Object;
import java.util.regex.*;
import au.com.bytecode.opencsv.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Converter {
	
    /*
        Consider a CSV file like the following:
        
        ID,Total,Assignment 1,Assignment 2,Exam 1
        111278,611,146,128,337
        111352,867,227,228,412
        111373,461,96,90,275
        111305,835,220,217,398
        111399,898,226,229,443
        111160,454,77,125,252
        111276,579,130,111,338
        111241,973,236,237,500
        
        The corresponding JSON file would be as follows (note the curly braces):
        
        {
            "colHeaders":["Total","Assignment 1","Assignment 2","Exam 1"],
            "rowHeaders":["111278","111352","111373","111305","111399","111160","111276","111241"],
            "data":[[611,146,128,337],
                    [867,227,228,412],
                    [461,96,90,275],
                    [835,220,217,398],
                    [898,226,229,443],
                    [454,77,125,252],
                    [579,130,111,338],
                    [973,236,237,500]
            ]
        }  
    */
    
    @SuppressWarnings("unchecked")
    public static String csvToJson(String csvString) {
		try{
			CSVReader reader = new CSVReader(new StringReader(csvString));
			List<String[]> array = reader.readAll();
			JSONArray colHeadersArray = new JSONArray();
			JSONArray rowHeadersArray = new JSONArray();
			JSONArray dataArray = new JSONArray();
			
			for(int i = 0; i < array.get(0).length; i++){colHeadersArray.add(array.get(0)[i]);}
			for(int i = 1; i < array.size(); i++){rowHeadersArray.add(array.get(i)[0]);}
			for(int i = 1; i < array.size(); i++){dataArray.add(Arrays.toString(Arrays.copyOfRange(array.get(i), 1, array.get(i).length)));}
			
			JSONObject newJSON = new JSONObject();
			newJSON.put("colHeaders", colHeadersArray);
			newJSON.put("rowHeaders", rowHeadersArray);
			newJSON.put("data", dataArray);
			
			String jsonFinal = "";
			jsonFinal = "{\n    \"colHeaders\":" + newJSON.get("colHeaders") + ",\n";
			jsonFinal += "    \"rowHeaders\":" + newJSON.get("rowHeaders") + ",\n";
			jsonFinal += "    \"data\":" + newJSON.get("data").toString().replaceAll("\"", "")
																		 .replaceAll("],", "],\n            ")
																		 .replaceAll(", ", ",")
																		 .replace("]]", "]\n    ]") + "\n}";
			return jsonFinal;
		}
		catch(IOException e){
			e.printStackTrace(System.out);
			return "ERROR";
		}
    }
    
    public static String jsonToCsv(String jsonString) {
		try{
			JSONObject jsonObj = new JSONObject();
			JSONParser parser = new JSONParser();
		
			jsonObj = (JSONObject)parser.parse(jsonString);
		
			JSONArray getColHeaders = (JSONArray)jsonObj.get("colHeaders");
			JSONArray getRowHeaders = (JSONArray)jsonObj.get("rowHeaders");
			JSONArray getData = (JSONArray)jsonObj.get("data");
			String csvFinal = "";
			for(int i = 0; i < getColHeaders.size(); i++){
				if(i == getColHeaders.size()-1){csvFinal += "\"" + getColHeaders.get(i).toString() + "\"\n";}
				else{csvFinal += "\"" + getColHeaders.get(i).toString() + "\"" + ",";}
			}
			for(int i = 0; i< getRowHeaders.size(); i++){
				csvFinal += "\"" + getRowHeaders.get(i).toString() + "\"" + ",";
				csvFinal += getData.get(i).toString().replaceAll("," , "\",\"")
													 .replace("[", "\"")
													 .replace("]", "\"\n");
			}
			return csvFinal;
		}
		catch(Exception e){
			e.printStackTrace(System.out);
			return "ERROR";
		}
    }
}













